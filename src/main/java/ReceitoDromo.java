package main.java;

import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * \file
 * \brief ReceitoDromo
 * \details Desktop application to view, add and manage recipes, ingredients and shopping lists
 * \author Tiago Dias
 * \date 01/07/2018
 * \bug No errors detected in the published version
 * \warning Do not change the .db file unless you know what you're doing
 * \version 1.0
 * \copyright GNU Public License
 */

public class ReceitoDromo {

    private JPanel main;
    private JTabbedPane tabbedPane1;
    private JTabbedPane recipes;
    private JPanel viewRecipe;
    private JPanel addIngredients;
    private JPanel tabRecipes;
    private JPanel tabIngredients;
    private JPanel tabShoppingList;
    private JComboBox comboBox1;
    private JTextArea viewSL;
    private JButton deleteShopList;
    private JTextArea addSL;
    private JButton addShopList;
    private JTabbedPane tabbedPane2;
    private JPanel addRecipe;
    private JPanel viewIngredients;
    private JTextField recipeName;
    private JSpinner dosageSpinner;
    private JSpinner prepSpinner;
    private JSpinner confSpinner;
    private JComboBox cbRecipeType;
    private JTextArea taRecipeIns;
    private JTextField tfRecipeObs;
    private JComboBox cbRecipeRating;
    private JTextArea taIngredients;
    private JButton btRecipeSubmit;
    private JTextField textField1;
    private JTable table1;
    private JTable table2;
    private JTextPane recipeInstr;
    private JTable table3;
    private JTable table4;
    private JButton button1;
    private JButton button2;
    private JButton button5;
    private JTextField textField2;
    private JButton button6;
    private JTextField textField6;
    private JLabel label1;
    private JFileChooser recipeFC;
    private JTextField textField3;
    private JSpinner spinner1;
    private JTextField spinner2;
    private JTextField textField4;
    private JButton button3;
    private JFileChooser ingFC;
    private JLabel ingPhoto;
    private JTable table5;
    private JTextField textField5;
    private Connection conn = null;

    /**
     * Class constructor
     */
    public ReceitoDromo() {
        connectDB();
        fillCalculator();
        fillComboBox();
        fillIngredients();
        resetComboBox();
        fillRecipeTable();
        /**
         * Set Shopping List combobox ActionEvent
         * Database - Selects ingredients from listacompras where data equals combobox selected item
         */
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String query2 = "select ingredientes from listacompras where data=?";
                    PreparedStatement pmst = conn.prepareStatement(query2);
                    pmst.setString(1, (String) comboBox1.getSelectedItem());
                    ResultSet rs = pmst.executeQuery();
                    while (rs.next()) {
                        String text = rs.getString(1);
                        viewSL.setText(text);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /**
         * Set Shopping List addShopList button MouseEvent
         * Database - Inserts data and ingredients into listacompras with current date
         */
        addShopList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    String query = "insert into listacompras (data, ingredientes) values (?, ?)";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, String.valueOf(dateFormat.format(date)));
                    preparedStatement.setString(2, addSL.getText());
                    preparedStatement.executeUpdate();
                    addSL.setText(null);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                resetComboBox();
            }
        });
        /**
         * Set Shopping List deleteShopList button MouseEvent
         * Database - Deletes from listacompras where data = combobox selected item
         */
        deleteShopList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    String query2 = "delete from listacompras where data=?";
                    PreparedStatement pmst = conn.prepareStatement(query2);
                    pmst.setString(1, (String) comboBox1.getSelectedItem());
                    pmst.executeUpdate();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                resetComboBox();
            }
        });
        /**
         * Set Add Recipe btRecipeSubmit MouseEvent
         * Database - Inserts all values from Add Recipe into receitas
         */
        btRecipeSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    String query = "insert into receitas (nome, doses, tempoprep, tempoconf, tipo, etapas, foto, observacoes, classificacao, ingredientes) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, recipeName.getText());
                    preparedStatement.setInt(2, (Integer) dosageSpinner.getValue());
                    preparedStatement.setInt(3, (Integer) prepSpinner.getValue());
                    preparedStatement.setInt(4, (Integer) confSpinner.getValue());
                    preparedStatement.setString(5, (String) cbRecipeType.getSelectedItem());
                    preparedStatement.setString(6, taRecipeIns.getText());
                    preparedStatement.setString(7, recipeFC.getSelectedFile().getPath());
                    preparedStatement.setString(8, tfRecipeObs.getText());
                    preparedStatement.setString(9, String.valueOf(cbRecipeRating.getSelectedItem()));
                    preparedStatement.setString(10, taIngredients.getText());
                    preparedStatement.executeUpdate();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }finally {
                    fillRecipeTable();
                }
            }
        });
        /**
         * Set View Recipe table2 MouseEvent
         * Database - Selects etapas and foto from receitas where nome = selected row from table2
         * Database - Selects values from ingredients to match with receitas
         */
        table2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                try {
                    String query = "select etapas, foto from receitas where nome= ?";
                    String value = String.valueOf(table2.getValueAt(table2.getSelectedRow(), 0));
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, value);
                    ResultSet rs = preparedStatement.executeQuery();
                    while (rs.next()) {
                        recipeInstr.setText(rs.getString(1));
                        label1.setIcon(new ImageIcon(rs.getString(2)));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                try {
                    DefaultTableModel model = new DefaultTableModel(new String[]{"Ingrediente", "Calorias", "Descricao", "Preco"}, 0) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String query = "select * from ingredientes, receitas where ingredientes.`relacionado` = receitas.`receitasid` and receitasid = ?";
                    PreparedStatement pmst = conn.prepareStatement(query);
                    String value = String.valueOf(table2.getModel().getValueAt(table2.getSelectedRow(), 0));
                    pmst.setString(1, value);
                    ResultSet rs = pmst.executeQuery();
                    while (rs.next()) {
                        String nome = rs.getString(2);
                        int calorias = rs.getInt(3);
                        String descricao = rs.getString(4);
                        String preco = rs.getString(5);
                        model.addRow(new Object[]{nome, calorias, descricao, preco});
                        table1.setModel(model);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /**
         * Set View Ingredients button2 MouseEvent
         * Appends selected value from table3 to addSL in Shopping List
         */
        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                String value = String.valueOf(table3.getModel().getValueAt(table3.getSelectedRow(), 0));
                addSL.append(value + " \n");
            }
        });
        /**
         * Set button1 Add to Calculator MouseEvent from View Ingredients
         * Database - Inserts values to calories table for calculating
         * Database - Selects all values from calorias to fill table4
         */
        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    String query = "insert into calorias(nome, valor, preco) values (?, ?, ?)";
                    String query2 = "select * from calorias";
                    DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Calorias", "Preco"}, 0) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String value = String.valueOf(table3.getValueAt(table3.getSelectedRow(), 0));
                    String value2 = String.valueOf(table3.getValueAt(table3.getSelectedRow(), 2));
                    double value3 = Double.parseDouble(String.valueOf(table3.getValueAt(table3.getSelectedRow(), 3)));
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, value);
                    preparedStatement.setString(2, value2);
                    preparedStatement.setDouble(3, value3);
                    preparedStatement.executeUpdate();
                    PreparedStatement preparedStatement2 = conn.prepareStatement(query2);
                    ResultSet resultSet = preparedStatement2.executeQuery();
                    while (resultSet.next()) {
                        defaultTableModel.addRow(new String[]{String.valueOf(resultSet.getString(1)), resultSet.getString(2), resultSet.getString(3)});
                        table4.setModel(defaultTableModel);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /**
         * Set textField1 SearchBar KeyEvent from View Recipes
         * Database - Searches table receitas for specific value and fills table2
         */
        textField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                searchPressedSound();
                DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"ID", "Nome", "Doses", "Preparacao", "Confecao", "Tipo", "Obs", "Classificacao"}, 0) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                try {
                    String value = String.valueOf(textField1.getText());
                    String query = "select * from receitas where nome LIKE ?";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, "%" + value + "%");
                    ResultSet rs = preparedStatement.executeQuery();
                    while (rs.next()) {
                        int id = rs.getInt(1);
                        String nome = rs.getString(2);
                        int doses = rs.getInt(3);
                        int tempoPrep = rs.getInt(4);
                        int tempoConf = rs.getInt(5);
                        String tipo = rs.getString(6);
                        String obs = rs.getString(9);
                        int classifi = rs.getInt(10);
                        defaultTableModel.addRow(new Object[]{id, nome, doses, tempoPrep, tempoConf, tipo, obs, classifi});
                        table2.setModel(defaultTableModel);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                table2.removeColumn(table2.getColumnModel().getColumn(0));
            }
        });
        /**
         * Set textField6 SearchBar KeyEvent from ViewIngredients
         * Database - Searches table ingredients for specific value and fills table3
         */
        textField6.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                searchPressedSound();
                DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Descricao", "Calorias"}, 0) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                };
                try {
                    String value = String.valueOf(textField6.getText());
                    String query = "select * from ingredientes where nome LIKE ?";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, "%" + value + "%");
                    ResultSet rs = preparedStatement.executeQuery();
                    while (rs.next()) {
                        String nome = rs.getString("nome");
                        String descricao = rs.getString("descricao");
                        int calorias = rs.getInt("calorias");
                        defaultTableModel.addRow(new Object[]{nome, descricao, calorias});
                        table3.setModel(defaultTableModel);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /**
         * Set table4 PropertyChangeEvent from View Ingredients
         * Database - Calculates calorias and preco to fill textField2 and textField5 respectively
         */
        table4.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                try {
                    String query = "select sum(valor) from calorias";
                    String query2 = "select sum(calorias.`preco`), ingredientes.`nome` from calorias, ingredientes where calorias.`nome` = ingredientes.`nome`";
                    Statement statement = conn.createStatement();
                    Statement statement1 = conn.createStatement();
                    ResultSet resultSet = statement.executeQuery(query);
                    ResultSet resultSet1 = statement1.executeQuery(query2);
                    textField2.setText(resultSet.getString(1));
                    textField5.setText(resultSet1.getString(1));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        /**
         * Set button6 Clear List MouseEvent from View Ingredients
         * Database- Deletes all rows from calorias
         * Database - Fills table4 with calorias
         */
        button6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Calorias"}, 0) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String query = "delete from calorias";
                    String query2 = "select * from calorias";
                    Statement statement = conn.createStatement();
                    statement.executeUpdate(query);
                    Statement statement1 = conn.createStatement();
                    ResultSet resultSet = statement1.executeQuery(query2);
                    while (resultSet.next()) {
                        defaultTableModel.addRow(new String[]{String.valueOf(resultSet.getString(1)), resultSet.getString(2)});
                        table4.setModel(defaultTableModel);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }finally {
                    fillCalculator();
                }
            }
        });
        /**
         * Set button5 Delete Items MouseEvent from View Ingredients
         * Database - Deletes row from calorias with a specific value
         * Database - Fills table4 with calorias
         */
        button5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Calorias"}, 0) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    String query = "delete from calorias where nome = ?";
                    String query2 = "select * from calorias";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, String.valueOf(table4.getValueAt(table4.getSelectedRow(), 0)));
                    preparedStatement.executeUpdate();
                    Statement statement = conn.createStatement();
                    ResultSet resultSet = statement.executeQuery(query2);
                    while (resultSet.next()) {
                        defaultTableModel.addRow(new String[]{String.valueOf(resultSet.getString(1)), resultSet.getString(2)});
                        table4.setModel(defaultTableModel);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
        /**
         * Set button3 MouseEvent from Add Ingredients
         * Database - Inserts add values into ingredients from Add Ingredients
         */
        button3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonPressedSound();
                try {
                    String query = "insert into ingredientes (nome, calorias, descricao, preco, foto) VALUES(?, ?, ?, ?, ?)";
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setString(1, textField3.getText());
                    preparedStatement.setInt(2, (Integer) spinner1.getValue());
                    preparedStatement.setString(3, textField4.getText());
                    preparedStatement.setDouble(4, Double.parseDouble(spinner2.getText()));
                    preparedStatement.setString(5, ingFC.getSelectedFile().getPath());
                    preparedStatement.executeUpdate();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }finally {
                    fillIngredients();
                }
            }
        });
        /**
         * Set table3 MouseEvent from View Ingredients
         * Database - Adds the photo of a specific ingredient to imgPhoto label
         * Database - Compares values and fills table5 with related items
         */
        table3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                try {
                    String query = "select foto from ingredientes where nome= ?";
                    String query2 = "select receitas.`nome`, receitas.`receitasid`, ingredientes.`nome`, ingredientes.`relacionado` from receitas, ingredientes where ingredientes.`nome` = ? and ingredientes.`relacionado` = receitas.`receitasid`";
                    String value = String.valueOf(table3.getValueAt(table3.getSelectedRow(), 0));
                    PreparedStatement preparedStatement = conn.prepareStatement(query);
                    PreparedStatement preparedStatement2 = conn.prepareStatement(query2);
                    preparedStatement.setString(1, value);
                    preparedStatement2.setString(1, value);
                    ResultSet resultSet = preparedStatement2.executeQuery();
                    while (resultSet.next()) {
                        DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Receita relacionada"}, 0) {
                            @Override
                            public boolean isCellEditable(int row, int column) {
                                return false;
                            }
                        };
                        defaultTableModel.addRow(new String[]{resultSet.getString(1)});
                        table5.setModel(defaultTableModel);

                    }
                    ResultSet rs = preparedStatement.executeQuery();
                    while (rs.next()) {
                        ingPhoto.setIcon(new ImageIcon(rs.getString(1)));
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    /**
     * Initialize JFrame
     *
     * @param args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            //WebLookAndFeel.install ();
            JFrame frame = new JFrame("ReceitoDromo");
            frame.setContentPane(new ReceitoDromo().main);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(600, 500);
            frame.setVisible(true);
        });
    }

    /**
     * Method to play typewriter sound on searchbar typing
     */
    private void buttonPressedSound() {
        final String soundName = "src/main/resources/media/audio/button_pressed.wav";
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to add sound for searchRecipe and searchIngredient textfields
     */
    private void searchPressedSound() {
        final String soundName = "src/main/resources/media/audio/search_pressed.wav";
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to fill the view ingredients table
     */
    private void fillIngredients() {
        try {
            DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Descricao", "Calorias", "Preco"}, 0) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            String query = "select * from ingredientes";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                resultSetMetaData.getColumnLabel(1);
                String nome = resultSet.getString(2);
                String descricao = resultSet.getString(2);
                int calorias = resultSet.getInt(3);
                String preco = resultSet.getString(5);
                defaultTableModel.addRow(new Object[]{nome, descricao, calorias, preco});
                table3.setModel(defaultTableModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to fill the Shopping List combobox
     */
    private void resetComboBox() {
        try {
            comboBox1.removeAllItems();
            String query = "select data from listacompras order by data desc";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String data = resultSet.getString(1);
                comboBox1.addItem(data);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        comboBox1.grabFocus();
    }

    /**
     * Method to fill the view recipe table
     */
    private void fillRecipeTable() {
        DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"ID", "Nome", "Doses", "Tempo Preparacao", "Tempo Confecao", "Tipo", "Observacoes", "Classificacao"}, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from receitas");
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String nome = resultSet.getString(2);
                int doses = resultSet.getInt(3);
                int tempoPrep = resultSet.getInt(4);
                int tempoConf = resultSet.getInt(5);
                String tipo = resultSet.getString(6);
                String obs = resultSet.getString(9);
                int classifi = resultSet.getInt(10);
                defaultTableModel.addRow(new Object[]{id, nome, doses, tempoPrep, tempoConf, tipo, obs, classifi});
                table2.setModel(defaultTableModel);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        table2.removeColumn(table2.getColumnModel().getColumn(0));
    }

    /**
     * Method to fill RecipeType and RecipeRating comboboxes
     */
    private void fillComboBox() {
        cbRecipeType.addItem("Doce");
        cbRecipeType.addItem("Peixe");
        cbRecipeType.addItem("Carne");
        cbRecipeType.addItem("Marisco");
        cbRecipeType.addItem("Sopa");
        cbRecipeRating.addItem(1);
        cbRecipeRating.addItem(2);
        cbRecipeRating.addItem(3);
        cbRecipeRating.addItem(4);
        cbRecipeRating.addItem(5);
    }

    /**
     * Method to fill table4 from View Ingredients
     */
    private void fillCalculator() {
        try {
            DefaultTableModel defaultTableModel = new DefaultTableModel(new String[]{"Nome", "Calorias", "Preco"}, 0) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            String query = "select * from calorias";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                defaultTableModel.addRow(new String[]{String.valueOf(resultSet.getString(1)), resultSet.getString(2), resultSet.getString(3)});
                table4.setModel(defaultTableModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to connect to the sqlite database
     */
    private void connectDB() {
        try {
            final String JDBC_DRIVER = "org.sqlite.JDBC";
            Class.forName(JDBC_DRIVER);
            System.out.println("Connecting to database...");
            final String DB_URL = "jdbc:sqlite:src/main/resources/db/receitodromo.db";
            conn = DriverManager.getConnection(DB_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
